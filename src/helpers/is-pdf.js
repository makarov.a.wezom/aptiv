const isPdf = resource => /\.pdf$/i.test(resource);
export default isPdf;
