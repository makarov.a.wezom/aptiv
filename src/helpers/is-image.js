const isImage = resource => /\.(png|jpe?g|gif|webp|tiff|bmp)$/i.test(resource);
export default isImage;
