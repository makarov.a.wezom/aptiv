export default function isIos() {
  return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream; // eslint-disable-line no-undef
}
