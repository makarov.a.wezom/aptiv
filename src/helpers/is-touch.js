export default function isTouch() {
  return (('ontouchstart' in window)
        || (navigator.MaxTouchPoints > 0) // eslint-disable-line no-undef
        || (navigator.msMaxTouchPoints > 0)); // eslint-disable-line no-undef
}
