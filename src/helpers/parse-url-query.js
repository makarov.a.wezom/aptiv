export default function parseQuery(queryString) {
  const query = {};
  const pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
  for (let i = 0; i < pairs.length; i++) {
    const pair = pairs[i].split('=');
    try {
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    } catch (e) {
      query[pair[0]] = '';
      console.warn(e);
    }
  }
  return query;
}
