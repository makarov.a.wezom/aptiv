export const sendMessageToSw = (msg) => {
  if (navigator.serviceWorker && navigator.serviceWorker.controller) { // eslint-disable-line no-undef
    navigator.serviceWorker.controller.postMessage(msg); // eslint-disable-line no-undef
  } else {
    console.warn('no sw installed yet');
  }
};

export const sendMessageToClient = (client, msg) => new Promise(((resolve, reject) => {
  const msgChan = new MessageChannel(); // eslint-disable-line no-undef

  msgChan.port1.onmessage = (event) => {
    if (event.data.error) {
      reject(event.data.error);
    } else {
      resolve(event.data);
    }
  };

  client.postMessage(`SW Says: '${msg}'`, [msgChan.port2]);
}));

export const sendMessageToAllClients = (msg) => {
  clients.matchAll().then((clients) => { // eslint-disable-line no-undef
    clients.forEach((client) => {
      sendMessageToClient(client, msg).then(m => console.log(`SW Received Message: ${m}`)); // eslint-disable-line no-console
    });
  });
};
