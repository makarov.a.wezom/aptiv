import React from 'react';
import { inject } from 'mobx-react';
import LoginForm from '~/containers/forms/LoginForm';
import LoginFormScheme from '~/containers/forms/LoginFormScheme';
import { withRouter } from 'react-router-dom';

@inject('modalsStore')
@withRouter
export default class LoginPageContainer extends React.Component {
    handleCancelClick = () => {
      this.props.history.push('/');
      this.props.modalsStore.rejectPrivacy();
    };
    render() {
      return (
        <LoginForm form={LoginFormScheme} onCancel={this.handleCancelClick} />
      );
    }
}
