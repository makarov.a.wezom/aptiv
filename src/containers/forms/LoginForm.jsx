import React from 'react';
import { inject, observer } from 'mobx-react';
import SimpleInput from '~/components/forms/SimpleInput';
import Button from '~/components/ui/Button';
import Icon from '~/components/ui/Icon';
import ArrowRightIcon from '~/style/svg/arrow-right.svg';
import PreloaderIcon from '~/style/svg/preloader.svg';

@inject('authStore')
@observer
export default class LoginForm extends React.Component {
  render() {
    return (
      <form onSubmit={this.props.form.onSubmit}>
        <SimpleInput field={this.props.form.$('username')} />
        <SimpleInput field={this.props.form.$('password')} type="password" />
        <div className="_text-center _mt-5">
          <Button
            className="button button--primary-o button--border-black"
            text="Start browsing"
            type="submit"
            disabled={this.props.authStore.pending}
            onClick={this.props.form.onSubmit}
            iconEnd={
                (this.props.form.submitting || this.props.form.validating) ?
                                (<Icon glyph={PreloaderIcon} style={{ width: '3.75em' }} />) :
                                (<Icon glyph={ArrowRightIcon} style={{ width: '3.75em' }} />)
                        }
          />
        </div>
        {this.props.form.errors() ? <p>{this.props.form.error}</p> : ''}
      </form>
    );
  }
}
