import React from 'react';
import { observer } from 'mobx-react';
import SimpleCheckbox from '~/components/forms/SimpleCheckbox';
import Button from '~/components/ui/Button';
import Icon from '~/components/ui/Icon';
import ArrowRightIcon from '~/style/svg/arrow-right.svg';
import PreloaderIcon from '~/style/svg/preloader.svg';

@observer
export default class PrivacyForm extends React.Component {
  render() {
    return (
      <form onSubmit={this.props.form.onSubmit}>
        <div className="_flex _justify-between _items-center">
          <SimpleCheckbox field={this.props.form.$('agree')} />
          <div className="_text-center">
            <Button
              className="button button--primary-o button--border-black"
              text="Sign in"
              type="submit"
              disabled={this.props.form.submitting || this.props.form.validating}
              onClick={this.props.form.onSubmit}
              iconEnd={
                                (this.props.form.submitting || this.props.form.validating) ?
                                    (<Icon glyph={PreloaderIcon} />) :
                                    (<Icon glyph={ArrowRightIcon} style={{ width: '3.75em' }} />)
                            }
            />
          </div>
        </div>
        {this.props.form.errors() ? this.props.form.error : ''}
      </form>
    );
  }
}
