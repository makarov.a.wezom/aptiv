import MobxReactForm from 'mobx-react-form';
import validatorjs from 'validatorjs';
import catalogStore from '~/stores/catalogStore';
// import toastrStore from '~/stores/toastrStore';

const plugins = { dvr: validatorjs };

const fields = [{
  name: 'emailTo',
  type: 'email',
  label: 'Enter e-mail',
  placeholder: 'email',
  rules: 'required|email',
}, {
  name: 'sendAll',
  type: 'checkbox',
  label: 'Send all files',
}, {
  name: 'fileId',
  type: 'hidden',
}, {
  name: 'productId',
  type: 'hidden',
}];

const hooks = {
  onSuccess(form) {
    return catalogStore.sendToEmail(form.values());
  },
};

export default new MobxReactForm({ fields }, { plugins, hooks });
