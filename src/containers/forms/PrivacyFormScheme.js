import MobxReactForm from 'mobx-react-form';
import validatorjs from 'validatorjs';
import authStore from '~/stores/authStore';
import toastrStore from '~/stores/toastrStore';

const plugins = { dvr: validatorjs };

const fields = [{
  name: 'agree',
  label: 'I agree',
  type: 'checkbox',
  rules: 'boolean|accepted',
}];

const hooks = {
  onSuccess(form) {
    if (form.values().agree) {
      return authStore.acceptPrivacy();
    }
    return toastrStore.container.warning('You need to accept the privacy policy');
  },
};

export default new MobxReactForm({ fields }, { plugins, hooks });
