import React from 'react';
import { inject } from 'mobx-react';
import ItemDownloadForm from '~/containers/forms/ItemDownloadForm';
import ItemDownloadScheme from '~/containers/forms/ItemDownloadScheme';
import { withRouter } from 'react-router-dom';

@inject('modalsStore')
@withRouter
export default class ItemDownloadContainer extends React.Component {
    handleCancelClick = () => {
      // this.props.history.push('/');
      // this.props.modalsStore.rejectPrivacy();
    };
    render() {
      return (
        <ItemDownloadForm form={ItemDownloadScheme} onCancel={this.handleCancelClick} {...this.props} />
      );
    }
}
