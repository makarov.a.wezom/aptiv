import React from 'react';
import { inject, observer } from 'mobx-react';
import SimpleInput from '~/components/forms/SimpleInput';
import SimpleCheckbox from '~/components/forms/SimpleCheckbox';
import Button from '~/components/ui/Button';
import Icon from '~/components/ui/Icon';
import ArrowRightIcon from '~/style/svg/arrow-right.svg';
import PreloaderIcon from '~/style/svg/preloader.svg';

@inject('catalogStore')
@observer
export default class ItemDownloadForm extends React.Component {
  componentDidMount() {
    this.props.form.$('fileId').set(this.props.fId);
    this.props.form.$('productId').set(this.props.productId);
  }
  render() {
    return (
      <form onSubmit={this.props.form.onSubmit}>
        <SimpleInput field={this.props.form.$('emailTo')} />
        <SimpleCheckbox field={this.props.form.$('sendAll')} />
        <SimpleInput className="_hide" field={this.props.form.$('fileId')} />
        <SimpleInput className="_hide" field={this.props.form.$('productId')} />
        <div className="_text-center _mt-5">
          <Button
            className="button button--primary-o button--border-black"
            text="Send"
            type="submit"
            disabled={this.props.catalogStore.emailPending}
            onClick={this.props.form.onSubmit}
            iconEnd={
                            (this.props.form.submitting || this.props.form.validating) ?
                                (<Icon glyph={PreloaderIcon} style={{ width: '3.75em' }} />) :
                                (<Icon glyph={ArrowRightIcon} style={{ width: '3.75em' }} />)
                        }
          />
        </div>
        {this.props.form.errors() ? <p>{this.props.form.error}</p> : ''}
      </form>
    );
  }
}
