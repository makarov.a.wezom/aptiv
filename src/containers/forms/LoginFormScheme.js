import MobxReactForm from 'mobx-react-form';
import validatorjs from 'validatorjs';
import AuthStore from '~/stores/authStore';
// import modalsStore from '~/stores/modalsStore';

const plugins = { dvr: validatorjs };

const fields = [{
  name: 'username',
  label: 'Enter your login',
  placeholder: 'email or login',
  rules: 'required|string|between:4,25',
}, {
  name: 'password',
  label: 'Enter your password',
  placeholder: 'password',
  rules: 'required|string|between:4,25',
}, {
  name: 'grant_type',
  label: 'grant_type',
  placeholder: 'grant_type',
  rules: 'required',
  value: 'password',
}];

const hooks = {
  onSuccess(form) {
    return AuthStore.login(form.values());
  },
};

export default new MobxReactForm({ fields }, { plugins, hooks });
