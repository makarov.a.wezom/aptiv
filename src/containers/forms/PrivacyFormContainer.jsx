import React from 'react';
import { inject } from 'mobx-react';
import PrivacyForm from '~/containers/forms/PrivacyForm';
import PrivacyFormScheme from '~/containers/forms/PrivacyFormScheme';
import { withRouter } from 'react-router-dom';

@inject('modalsStore')
@withRouter
export default class PrivacyFormContainer extends React.Component {
    handleCancelClick = () => {
      this.props.history.push('/');
      this.props.modalsStore.rejectPrivacy();
    };
    render() {
      return (
        <PrivacyForm form={PrivacyFormScheme} onCancel={this.handleCancelClick} />
      );
    }
}
