import React from 'react';
// import { action, observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import ItemInnerList from '~/components/ItemInnerList';
// import Icon from '~/components/ui/Icon';
// import DescIcon from '~/style/svg/desc.svg';

@withRouter
@inject('catalogStore')
@observer
export default class ProductPageContainer extends React.Component {
  componentDidMount() {
    if (!this.props.catalogStore.categoriesLoaded) {
      this.props.catalogStore.fetchItems();
    }
  }

  render() {
    const { match: { params: { productId } } } = this.props;
    return (
      <div>
        {
            this.props.catalogStore.productsItemsById[productId] ? (
              <div>
                <div className="_flex _justify-between _items-center">
                  <div className="header-title">{this.props.catalogStore.productsItemsById[productId].name}</div>
                </div>
                <ItemInnerList item={this.props.catalogStore.productsItemsById[productId]} />
              </div>
            ) : ''
        }
      </div>
    );
  }
}
