import React from 'react';
import { Helmet } from 'react-helmet';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import { ToastContainer, ToastMessageAnimated } from 'react-toastr';
import Header from '~/components/Header';
import Breadcrumbs from '~/components/Breadcrumbs';
import Preloader from '~/components/ui/Preloader';

@withRouter
@inject('toastrStore', 'routing', 'catalogStore')
@observer
export default class BaseLayout extends React.Component {
  componentWillReceiveProps() {
    this.forceUpdate();
  }
    setToastrRef = (ref) => {
      this.props.toastrStore.setContainer(ref);
    };

    render() {
      const pageSizeClasses = this.props.routing.location.pathname === '/' ? ['page-main--index-bg'] : [''];

      const {
        title, background,
      } = this.props;

      return (
        <div className="page-wrapper" style={{ background }}>
          <Helmet>
            <title>Aptiv {title ? ` | ${title}` : ''}</title>
          </Helmet>
          <ToastContainer
            ref={this.setToastrRef}
            className="toast-top-right"
            toastMessageFactory={React.createFactory(ToastMessageAnimated)}
          />
          <div className={`page-main ${pageSizeClasses.join(' ')}`}>
            <div className="page-size">
              {
                  this.props.routing.location.pathname !== '/' ?
                [
                  (<Header key={1} />),
                ]
                      : ''
              }
              {
                  this.props.routing.location.pathname !== '/' && !this.props.catalogStore.categoriesPending ? (<Breadcrumbs key={2} />) : ''
              }
              {
                  this.props.pending ?
                  (
                    <div className="_flex _flex-grow-1 _w-100 _justify-center _items-center" style={{ minHeight: '20vh' }}>
                      <Preloader />
                    </div>
                  ) : null
              }
              <div className={`${this.props.pending ? '_hide' : ''}`}>{this.props.children}</div>
            </div>
          </div>
        </div>
      );
    }
}
