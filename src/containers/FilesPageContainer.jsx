import React from 'react';
import { inject, observer } from 'mobx-react';
import { action, observable } from 'mobx';

@inject('modalsStore', 'catalogStore')
@observer
export default class FilesPageContainer extends React.Component {
  componentDidMount() {
    if (this.props.catalogStore.categoriesLoaded) {
      this.initSlides();
    } else {
      this.props.catalogStore.fetchItems().then(this.initSlides);
    }
  }
    @action setSlides = (slides = []) => {
      this.slides.replace(slides);
    };
    @action initSlides = () => {
      const { match: { params } } = this.props;
      const { productId, resourceType } = params;
      this.setSlides(this.props.catalogStore.productsItemsById[productId][resourceType]);
    };
    @observable slides = [];

    render() {
      return (
        <div>
          <div className="header-title header-title--ins">{this.props.type}</div>
          <div className="header-title header-title--sub">{this.props.title}</div>
          <div className="item-slider">
            {
                        this.slides.map((slide, index) => (
                          <div className="item-slider__current-text _pt-3 _pb-3" key={index}>
                            <span className="item-slider__text">File name:</span>
                            <span className="item-slider__text-value _wb-ba">{slide.fileName}</span>
                          </div>
                        ))
                    }
          </div>
        </div>
      );
    }
}
