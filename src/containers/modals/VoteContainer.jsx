import React from 'react';
import { observable, action } from 'mobx';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import Button from '~/components/ui/Button';
import Icon from '~/components/ui/Icon';
import PreloaderIcon from '~/style/svg/preloader.svg';

@withRouter
@inject('modalsStore', 'catalogStore')
@observer
export default class VoteContainer extends React.Component {
    @observable pending = false;
    @action handleCancelClick = () => {
      this.props.modalsStore.closeModal();
    };
    @action handleVote = () => {
      this.pending = true;
      const { voteModalParams } = this.props.modalsStore;

      const voteFn = this.props.catalogStore[`${this.props.namespace}Vote`] || this.props.catalogStore.productsVote;

      return voteFn(voteModalParams.item).then(() => {
        this.props.modalsStore.closeModal();
      }).finally(() => {
        this.pending = false;
      });
    };

    render() {
      return (
        <div className="_text-center _mt-5">
          <div className="_mb-3">Are you sure you want to vote? Votes will help our team prioritize development of
                    new marketing materials
          </div>
          <Button
            className="button _mr-3"
            text="Cancel"
            onClick={this.handleCancelClick}
          />
          <Button
            className="button button--primary-o button--border-black"
            text="Vote"
            disabled={this.props.pending || this.pending}
            onClick={this.handleVote}
            iconEnd={
                        (this.props.pending || this.pending)
                            ? (<Icon glyph={PreloaderIcon} style={{ width: '3.75em' }} />)
                            : ''
                    }
          />
        </div>
      );
    }
}
