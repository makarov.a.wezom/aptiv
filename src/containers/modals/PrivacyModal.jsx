import React from 'react';
import { inject, observer } from 'mobx-react';
import Modal from '~/components/ui/Modal';
import PrivacyForm from '~/containers/forms/PrivacyFormContainer';

@inject('authStore')
@observer
export default class PrivacyModal extends React.Component {
  render() {
    const { isOpen, onCloseModal } = this.props;
    return (
      <Modal
        isOpen={isOpen}
        onCloseModal={onCloseModal}
        title="Privacy policy"
                /* eslint-disable */
                content={
                    [
                        (
                            <div key={1}>
                                <div className="wysiwyg text-block styled-scroll _mb-5" dangerouslySetInnerHTML={{__html: this.props.authStore.meta.policy}}/>
                            </div>
                        ),
                        (
                            <PrivacyForm key={2}/>
                        )
                    ]
                }
            />
        );
    }
};
