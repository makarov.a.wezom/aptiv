import React from 'react';
import { observer } from 'mobx-react';
import Modal from '~/components/ui/Modal';
import ItemDownloadContainer from '~/containers/forms/ItemDownloadContainer';

export default observer(({
  isOpen, onCloseModal, filename, fId, productId,
}) =>
  (
    <Modal
      isOpen={isOpen}
      onCloseModal={onCloseModal}
      title={
        <div>
          <div>FILE NAME:</div>
          <div className="_wb-ba">{filename}</div>
        </div>
      }
      content={<ItemDownloadContainer productId={productId} fId={fId} />}
    />
  ));
