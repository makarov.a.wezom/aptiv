import React from 'react';
import { observer } from 'mobx-react';
import Modal from '~/components/ui/Modal';
import LoginFormContainer from '~/containers/forms/LoginFormContainer';

export default observer(({ isOpen, onCloseModal }) =>
  (
    <Modal
      isOpen={isOpen}
      onCloseModal={onCloseModal}
      title="Enter your login and password"
      content={<LoginFormContainer />}
    />
  ));
