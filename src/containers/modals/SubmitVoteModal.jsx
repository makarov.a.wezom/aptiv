import React from 'react';
import { observer } from 'mobx-react';
import Modal from '~/components/ui/Modal';
import VoteContainer from '~/containers/modals/VoteContainer';

export default observer(({
  isOpen, onCloseModal, categoryId, productId, namespace,
}) =>
  (
    <Modal
      isOpen={isOpen}
      onCloseModal={onCloseModal}
      className="react-modal__modal--small"
      title={<div>Are you sure?</div>}
      content={<VoteContainer
        namespace={namespace}
        categoryId={categoryId}
        productId={productId}
      />}
    />
  ));
