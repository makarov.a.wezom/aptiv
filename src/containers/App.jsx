import React from 'react';
import { hot } from 'react-hot-loader';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import Routes from '~/router/routes';

@inject('userStore', 'commonStore', 'authStore')
@withRouter
@observer
class AppRoot extends React.Component {
  componentWillMount() {
    if (!this.props.commonStore.token) {
      this.props.commonStore.setAppLoaded();
    }
    this.props.authStore.checkRequiredAuth();
  }

  componentDidMount() {
    if (this.props.commonStore.token) {
      this.props.userStore.pullUser();
      this.props.commonStore.setAppLoaded();
    }
  }

  componentWillUpdate(nextProps) {
    if (!nextProps.commonStore.token) {
      this.props.history.push('/');
    }
  }


  render() {
    if (this.props.commonStore.appLoaded) {
      return <Routes />;
    }

    return '';
  }
}

let App; //eslint-disable-line

// TODO: fix hmr

if (process.env.NODE_ENV === 'production') {
  App = AppRoot;
} else {
  App = hot(module)(AppRoot);
}

export default App;
