import React from 'react';
// import { action, observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import ReactDOM from 'react-dom';
import Slider from 'react-slick';
import { action, observable } from 'mobx';
import SliderArrow from '~/components/ui/Slider-arrow';
import ItemSlide from '~/components/Item-slide';
import ItemDownloadModal from '~/containers/modals/ItemDownloadModal';
import FullScreenIcon from '~/components/ui/Full-screen-icon';
import Tooltip from '~/components/ui/Tooltip';
import CustomFullScreen from '~/components/CustomFullScreen';
import Icon from '~/components/ui/Icon';
import DownloadIcon from '~/style/svg/download.svg';
import MailIcon from '~/style/svg/mail.svg';
import { API_PATH } from '~/config/config';
import isPdf from '../helpers/is-pdf';
import isImage from '../helpers/is-image';
import isIos from '../helpers/is-ios';

@withRouter
@inject('modalsStore', 'catalogStore')
@observer
export default class ResourcesPageContainer extends React.Component {
  componentDidMount() {
    const { match: { params } } = this.props;
    if (!this.props.catalogStore.categoriesLoaded) {
      this.props.catalogStore.fetchItems().then(() => {
        const product = this.props.catalogStore.productsItemsById[params.productId];
        this.setSlides(product[params.resourceType]);
      });
    } else {
      const product = this.props.catalogStore.productsItemsById[params.productId];
      this.setSlides(product[params.resourceType]);
    }
  }

  get fullscreenUrl() {
    return isIos() ?
      `https://docs.google.com/gview?url=${this.slides[this.selectedIndex].urlOriginal}&embedded=true` :
      this.slides[this.selectedIndex].urlOriginal;
  }

    getImgUrl = item => (!isImage(item.urlOriginal) ? item.urlThumbnailSlider : item.urlOriginal);

    @action setSlides = (slides = []) => {
      this.slides.replace(slides);
    };
    @action handleSetRef = (ref, id) => {
      this.references[id] = ref;
    };
    @action handleAfterChange = (index) => {
      this.selected = this.slides[index].id;
      this.selectedIndex = index;
    };
    @action handleFullScreenClick = () => {
      const { match: { params } } = this.props;
      const { resourceType } = params;
      if (resourceType === 'images' || !isPdf(this.slides[this.selectedIndex].urlOriginal)) {
            const docElm = ReactDOM.findDOMNode(this.references[this.selected]); // eslint-disable-line
        if (docElm.requestFullscreen) docElm.requestFullscreen();
        else if (docElm.msRequestFullscreen) docElm.msRequestFullscreen();
        else if (docElm.mozRequestFullScreen) docElm.mozRequestFullScreen();
        else if (docElm.webkitRequestFullScreen) docElm.webkitRequestFullScreen();
        else {
          this.goFull();
        }
      } else {
        this.goFull();
        // window.open(this.slides[this.selectedIndex].urlOriginal, '_self', 'fullscreen=yes');
      }
    };

    @action handleSliderInit = () => {
      this.selected = this.slides[0].id || 0;
    };

    @action openSendToEmail = () => {
      this.props.modalsStore.openModal('ItemDownload');
    };

    @action goFull = () => {
      this.isFull = true;
    };
    @action handleFullscreenChange = (isFull) => {
      this.isFull = isFull;
    };
    @action handleCloseFullscreen = () => { // eslint-disable-line no-unsed-vars
      this.isFull = false;
    };

    @observable selected = 0;
    @observable selectedIndex = 0;
    @observable references = {};
    @observable slides = [];
    @observable isFull = false;


    render() {
      const { match: { params } } = this.props;
      const pagesDiff = 2;

      const ln = this.slides.length;
      const MAX_SLIDES_PER_PAGING = 6;
      const showFullPaging = ln > MAX_SLIDES_PER_PAGING;

      const pagingComponent = (i) => {
        const val = i + 1;
        const strVal = val < 10 ? `0${val}` : val;
        const index = this.selectedIndex;
        if (showFullPaging && (i - pagesDiff > index || i + pagesDiff < index) && !(i === 0 || val === this.slides.length)) return <span />;
        return (<div className="item-slider__pagination-item">{strVal}</div>);
      };

      const dotsComponent = (dots, className) => (
        <div key={1} className={className}>
          <ul className={
                    `
                item-slider__pagination-items
                _self-start _sm-self-center
                ${this.selectedIndex - pagesDiff > 1 && showFullPaging ? 'item-slider__pagination-items--has-left' : ''}
                ${this.selectedIndex + pagesDiff < ln && showFullPaging ? 'item-slider__pagination-items--has-right' : ''}
                 grid grid--space-def
                 `
                }
          >
            {dots}
          </ul>
          <div>
            <div className="grid grid--space-def">
              <div className="gcell">
                <Tooltip
                  element={(
                    <div
                      className="button button--without-bd button--rounded button--only-icon c-pointer"
                      onClick={this.openSendToEmail}
                      role="presentation"
                    >
                      <span><Icon glyph={MailIcon} style={{ width: '100%', height: 'auto' }} /></span>
                    </div>
                  )}
                  tooltipContent="Send to email"
                  direction="top"
                />
              </div>
              <div className="gcell">
                <Tooltip
                  element={(
                    <a
                      href={`${API_PATH}/api/products/${params.productId}/document/${this.slides[this.selectedIndex].id}`}
                      className="button button--without-bd button--rounded button--only-icon c-pointer"
                      download
                    >
                      <span>
                        <Icon
                          glyph={DownloadIcon}
                          style={{ width: '100%', height: 'auto' }}
                        />
                      </span>
                    </a>
                      )}
                  tooltipContent="Download file"
                  direction="top"
                  enableClicks
                />
              </div>
            </div>
          </div>
        </div>
      );

      const fakeDots = (
        <li className="slick-active">{pagingComponent(0)}</li>
      );

      const dotsComponentClasses = 'item-slider__pagination-wrapper _pt-2 _pb-2 _flex _items-center _flex-column _sm-flex-row _justify-between';

      const settings = {
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        initialSlide: 0,
        prevArrow: <SliderArrow additionalClassName="item-slider__arrow item-slider__arrow--left" />,
        nextArrow: <SliderArrow additionalClassName="item-slider__arrow item-slider__arrow--right" />,
        className: 'item-slider__slider',
        dots: true,
        dotsClass: dotsComponentClasses,
        appendDots: dotsComponent,
        customPaging: pagingComponent,
        lazyLoad: 'progressive',
        afterChange: this.handleAfterChange,
        onInit: this.handleSliderInit,
      };

      return (
        <div>
          {
            this.slides.length ? (
              <div>
                <CustomFullScreen
                  enabled={this.isFull}
                  onChange={this.handleFullscreenChange}
                  onCloseClick={this.handleCloseFullscreen}
                >
                  {isPdf(this.slides[this.selectedIndex].urlOriginal) ? (<iframe
                    className="fullscreen__iframe"
                    title="fullscreen-preview"
                    scrolling="no"
                    allowFullScreen
                    webkitallowfullscreen="true"
                    src={this.fullscreenUrl}
                    style={!this.isFull ? { display: 'none' } : {}}
                  />) : <img src={this.getImgUrl(this.slides[this.selectedIndex])} alt="" />}
                </CustomFullScreen>
                <div className="header-title header-title--ins">{this.props.type}</div>
                <div className="header-title header-title--sub">{this.props.title}</div>
                <div className={`item-slider ${this.slides.length === 1 ? 'item-slider--single' : ''}`}>
                  <Slider {...settings}>
                    {
                      this.slides.map(slide =>
                        (
                          <ItemSlide
                            crop={params.resourceType !== 'images'}
                            key={slide.id}
                            item={slide}
                            url={this.isFull ? this.getImgUrl(slide) : slide.urlThumbnailSlider}
                            onSetRef={this.handleSetRef}
                          />))
                    }
                  </Slider>
                  {
                      this.slides.length === 1 ? dotsComponent(fakeDots, `${dotsComponentClasses} item-slider__pagination-wrapper--single`) : ''
                  }
                  <div className="item-slider__current-text _pt-3 _pb-3">
                    <span className="item-slider__text">{this.slides[this.selectedIndex].description ? 'Description' : 'File name'}:</span>
                    <span className="item-slider__text-value _wb-ba">
                      {this.slides[this.selectedIndex].description || this.slides[this.selectedIndex].fileName}
                    </span>
                  </div>
                  <Tooltip
                    element={
                      <FullScreenIcon
                        isLink
                        reference={this.references[this.selected]}
                        onClick={this.handleFullScreenClick}
                      />
                    }
                    tooltipContent="Go fullscreen"
                    enableClicks
                    direction="left"
                    style={{
                        position: 'absolute',
                        right: '0',
                        top: '0',
                    }}
                  />
                </div>
                <ItemDownloadModal
                  isOpen={this.props.modalsStore.openedModal === 'ItemDownload'}
                  onCloseModal={() => this.props.modalsStore.closeModal()}
                  filename={this.slides[this.selectedIndex || 0].fileName}
                  fId={this.slides[this.selectedIndex || 0].id}
                  productId={params.productId}
                />
              </div>
          ) : (<div className="_mt-2">No data found</div>)
          }
        </div>
      );
    }
}
