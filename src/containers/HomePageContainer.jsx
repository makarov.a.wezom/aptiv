import React from 'react';
// import { action, observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import LoginModal from '~/containers/modals/LoginModal';
import PrivacyModal from '~/containers/modals/PrivacyModal';
import Logo from '~/components/Logo';
import Button from '~/components/ui/Button';
import Icon from '~/components/ui/Icon';
import ArrowRightIcon from '~/style/svg/arrow-right.svg';
import PreloaderIcon from '~/style/svg/preloader.svg';
import isIE11 from '../helpers/is-ie11';

@inject('modalsStore', 'authStore', 'toastrStore', 'routing')
@observer
export default class HomePageContainer extends React.Component {
    handleStartClick = () => {
      if (this.props.authStore.loggedIn) {
        this.props.routing.push('/product-lines');
      } else {
        this.props.authStore.checkAuth();
      }
    };
    render() {
      return (
        <div>
          <Logo className="header__logo--homepage" />
          <div className="grid _md-justify-end">
            <div className="gcell--12 gcell--md-7 _md-text-right">
              <div className="title title--home-page _color-white-2">Product literature an email away</div>
              <div className={isIE11() ? '_ovh' : ''}>
                <Button
                  className="button button--primary-o button--border-gray"
                  text="Start browsing"
                  onClick={this.handleStartClick}
                  disabled={
                      this.props.authStore.checkAuthProgress ||
                      this.props.authStore.privacyPending
                  }
                  iconEnd={
                            this.props.authStore.checkAuthProgress || this.props.authStore.privacyPending ?
                                (<Icon glyph={PreloaderIcon} style={{ width: '3.75em' }} />) :
                                (<Icon glyph={ArrowRightIcon} style={{ width: '3.75em' }} />)
                        }
                />
              </div>
              <div className="grid _md-justify-end">
                <div className="simple-text simple-text--spaced _color-white-2 gcell--10">
                                Datasheets, presentations, images,
                                catalogues, brochures. Email them
                                to yourself or your customers from right here.
                </div>
              </div>
            </div>
          </div>
          <LoginModal
            isOpen={this.props.modalsStore.openedModal === 'login'}
            onCloseModal={() => this.props.modalsStore.closeModal()}
          />
          <PrivacyModal
            isOpen={this.props.modalsStore.openedModal === 'privacy'}
            onCloseModal={() => this.props.modalsStore.closeModal()}
          />
        </div>
      );
    }
}
