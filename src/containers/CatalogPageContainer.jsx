import React from 'react';
import { action, observable } from 'mobx';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
// import BaseLayout from '~/containers/BaseLayout';
import FullScreenBtn from '~/components/FullScreenBtn';
import Tooltip from '~/components/ui/Tooltip';
import CustomFullScreen from '~/components/CustomFullScreen';
import CatalogItemsList from '~/components/CatalogItemsList';
import SubmitVoteModal from '~/containers/modals/SubmitVoteModal';

@withRouter
@inject('catalogStore', 'modalsStore')
@observer
export default class CatalogPageContainer extends React.Component {
  componentDidMount() {
    if (!this.props.catalogStore.categoriesLoaded || this.props.namespace === 'search') {
      this.props.refresh();
    }
  }

    handleDownloadData = () => this.props.catalogStore.downloadSchedule();

    @action handleVote = () => {
      this.props.modalsStore.openModal('submitVote');
    };
    @action handleFullscreenChange = (isFull) => {
      this.isFull = isFull;
    };
    @action goFull = () => {
      this.isFull = true;
    };
    @action handleCloseFullscreen = () => { // eslint-disable-line no-unsed-vars
      this.isFull = false;
    };
    @observable isFull = false;

    render() {
      const { match: { params } } = this.props;
      const items = this.props.catalogStore.categoriesLoaded ?
        this.props.namespace === 'categories' ? this.props.catalogStore.categoriesItems :
          this.props.namespace === 'search' ? this.props.catalogStore.searchItems :
            this.props.catalogStore.categoriesItemsById && this.props.catalogStore.categoriesItemsById[params.categoryId] ?
              this.props.catalogStore.categoriesItemsById[params.categoryId].products :
              [] :
        [];

      return ( // eslint-disable-line no-return-assign
        <div>
          {items.length && this.props.catalogStore.meta && this.props.catalogStore.meta.scheduleFileLink ? (
            <CustomFullScreen
              enabled={this.isFull}
              onChange={this.handleFullscreenChange}
              onCloseClick={this.handleCloseFullscreen}
            >
              <iframe
                className="fullscreen__iframe"
                title="fullscreen-preview"
                allowFullScreen
                scrolling="no"
                webkitallowfullscreen="true"
                src={`https://docs.google.com/gview?url=${this.props.catalogStore.meta.scheduleFileLink}&embedded=true`}
              />
            </CustomFullScreen>
                ) : ''}
          <div className="_flex _justify-between _items-center">
            <div className="header-title">
              {
                  this.props.namespace === 'products' &&
                  this.props.catalogStore.categoriesLoaded &&
                  this.props.catalogStore.categoriesItemsById[params.categoryId] ?
                      this.props.catalogStore.categoriesItemsById[params.categoryId].name :
                      this.props.title
              }
            </div>
            {
                this.props.catalogStore.meta && this.props.catalogStore.meta.scheduleFileLink ? (
                  <Tooltip
                    element={<FullScreenBtn onClick={this.goFull} link={this.props.catalogStore.meta.scheduleFileLink} />}
                    tooltipContent="Open event calendar"
                    direction="left"
                  />
                ) : ''
            }
          </div>
          {
              !this.props.catalogStore.categoriesError ?
                  items.length ?
                      (
                        <CatalogItemsList
                          hide={this.isFull}
                          namespace={this.props.namespace}
                          items={items}
                          onVote={this.handleVote}
                        />
                      ) :
                      (<span>No data</span>)
                  : (
                    <span>Request failed...
                      <span
                        role="presentation"
                        onClick={this.props.refresh}
                        className="_color-primary _upper c-pointer"
                      >refresh
                      </span>
                    </span>
                  )
          }
          <SubmitVoteModal
            isOpen={this.props.modalsStore.openedModal === 'submitVote'}
            onCloseModal={() => this.props.modalsStore.closeModal()}
            productId={params.productId}
            categoryId={params.categoryId}
            namespace={this.props.namespace}
          />
        </div>
      );
    }
}
