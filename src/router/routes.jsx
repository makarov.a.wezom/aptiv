// import config from '../config';
import React from 'react';
// import modernizr from 'modernizr';
import {
  Route,
  // Switch,
} from 'react-router-dom';
import { withRouter } from 'react-router';
import { inject, observer } from 'mobx-react';
import { AnimatedSwitch } from 'react-router-transition';
// spring
import BaseLayout from '~/containers/BaseLayout';
import HomePageContainer from '~/containers/HomePageContainer';
import CatalogPageContainer from '~/containers/CatalogPageContainer';
import ProductPageContainer from '~/containers/ProductPageContainer';
import ResourcesPageContainer from '~/containers/ResourcesPageContainer';
import UiPageContainer from '~/containers/UiPageContainer';
import NoMatchPageContainer from '~/containers/NoMatchPageContainer';
import catalogStore from '~/stores/catalogStore';
import parseQuery from '../helpers/parse-url-query';
// import isMobile from '../helpers/is-mobile';
// import isTouch from '../helpers/is-touch';

const SearchPageContainer = withRouter((props) => {
  const values = parseQuery(props.location.search);
  return (
    <CatalogPageContainer
      namespace="search"
      refresh={() => catalogStore.fetchSearch(values.text || '')}
    />
  );
});

const CategoriesPageContainer = () => (<CatalogPageContainer title="Product lines" namespace="categories" refresh={() => catalogStore.fetchItems()} />);
const ProductsPageContainer = () => (<CatalogPageContainer namespace="products" refresh={() => catalogStore.fetchItems()} />);

// import PrivateRoute from '~/components/PrivateRoute';

function mapStyles(styles) {
  return { ...styles };
}

/* const bounceTransition = {
  atEnter: {
    opacity: 0,
    transform: -50,
  },
  atLeave: {
    opacity: 0,
    transform: 50,
  },
  atActive: {
    opacity: 1,
    transform: 0,
  },
}; */

const bounceTransitionMobile = {
  atEnter: {},
  atLeave: {},
  atActive: {},
};

/*
function mapStyles(styles) {
  return !isMobile() ? {
    opacity: styles.opacity,
    transform: `translate3d(${styles.transform}px,0,0)`,
  } : { ...styles };
}

atEnter={!isMobile() ? bounceTransition.atEnter : bounceTransitionMobile.atEnter}
atLeave={!isMobile() ? bounceTransition.atLeave : bounceTransitionMobile.atLeave}
atActive={!isMobile() ? bounceTransition.atActive : bounceTransitionMobile.atActive}
*/

@withRouter
@inject('catalogStore', 'modalsStore')
@observer
export default class Routes extends React.Component { // eslint-disable-line
  render() {
    return (
      <BaseLayout
        pending={this.props.catalogStore.categoriesPending}
      >
        <AnimatedSwitch
          className="switch-wrapper"
          mapStyles={mapStyles}
          atEnter={bounceTransitionMobile.atEnter}
          atLeave={bounceTransitionMobile.atLeave}
          atActive={bounceTransitionMobile.atActive}
        >
          <Route exact path="/" component={HomePageContainer} />
          <Route
            exact
            path="/product-lines"
            component={CategoriesPageContainer}
          />
          <Route
            path="/product-lines/search"
            component={SearchPageContainer}
          />
          <Route
            exact
            path="/product-lines/:categoryId([0-9]+)"
            component={ProductsPageContainer}
          />
          <Route
            exact
            path="/product-lines/:categoryId([0-9]+)/products/:productId([0-9]+)"
            component={ProductPageContainer}
          />
          <Route exact path="/product-lines/:categoryId([0-9]+)/products/:productId([0-9]+)/:resourceType" component={ResourcesPageContainer} />
          <Route exact path="/product-lines/products/:productId([0-9]+)" component={ProductPageContainer} />
          <Route exact path="/product-lines/products/:productId([0-9]+)/:resourceType" component={ResourcesPageContainer} />
          {process.env.NODE_ENV === 'production' ? '' : (<Route exact path="/ui" component={UiPageContainer} />)}
          <Route component={NoMatchPageContainer} />
        </AnimatedSwitch>
      </BaseLayout>
    );
  }
}
