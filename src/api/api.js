import axiosInstance from '~/config/axios';

const Auth = {
  login: (data) => {
    const params = new URLSearchParams();
    Object.keys(data).forEach((el) => {
      params.append(el, data[el]);
    });
    return axiosInstance.post('token', params, {
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
    });
  },
  refreshAuthToken: (refreshToken) => {
    const params = new URLSearchParams();
    params.append('refresh_token', refreshToken);
    params.append('grant_type', 'refresh_token');
    return axiosInstance.post('token', params, {
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
    });
  },
  checkAuth: () => axiosInstance.post('api/auth/check'),
  acceptPrivacy: params => axiosInstance.post('api/policy', { ...params }),
  getPolicyText: () => axiosInstance.get('api/policy'),
  logout: () => axiosInstance.post('api/logout'),
};

const Catalog = {
  groupsCache: () => axiosInstance.get('api/groups/cache'),
  groups: () => axiosInstance.get('api/groups'),
  group: groupId => ({
    products: () => axiosInstance.get(`api/groups/${groupId}/products`),
    product: productId => axiosInstance.get(`api/groups/${groupId}/products/${productId}`),
  }),
  products: () => ({
    get: productId => axiosInstance.get(`api/products/${productId}`),
  }),
  categoriesVote: id => axiosInstance.post(`api/groups/vote/${id}`),
  productsVote: id => axiosInstance.post(`api/products/vote/${id}`),
  sendToEmail: (productId, params) => axiosInstance.post(`api/products/${productId}/send`, { ...params }),
  search: params => axiosInstance.get('api/products/search', {
    params: {
      text: params,
    },
  }),
};

export default {
  Auth,
  Catalog,
};
