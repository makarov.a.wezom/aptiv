import React from 'react';
import { inject, observer } from 'mobx-react';
// import { observable } from 'mobx';

import Logo from '~/components/Logo';
import Button from '~/components/ui/Button';
import SearchInput from '~/components/ui/Search-input';

@inject('authStore', 'catalogStore')
@observer
export default class Header extends React.Component {
  render() {
    return (
      <div className="_flex _flex-column _md-flex-row _items-center _justify-between header">
        <Logo />
        <div className="_flex-grow-1 _my-2 _md-ml-5 _md-mr-2 _md-my-0 _w-100 _md-w-auto">
          <SearchInput />
        </div>
        {
                this.props.authStore.requireAuth ?
                (
                  <Button
                    className="button button--without-bd button--link white-on-hover"
                    text="Logout"
                    onClick={() => this.props.authStore.logout()}
                  />
                ) : ''
           }
      </div>
    );
  }
}
