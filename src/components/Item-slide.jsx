import React from 'react';
import { observer } from 'mobx-react';
import { observable, action } from 'mobx';

@observer
export default class ItemSlide extends React.Component {
    @action setRef = (node) => { this.imgRef = node; this.props.onSetRef(node, this.props.item.id); };
    @observable.ref imgRef = null;
    render() {
      const {
        crop, url,
      } = this.props;
      return (
        <div
          className={`item-slider__item ${crop ? 'item-slider__item--cropped' : ''}`}
          ref={this.setRef}
          style={{ backgroundImage: `url(${url})` }} // eslint-disable-line
        />
      );
    }
}
