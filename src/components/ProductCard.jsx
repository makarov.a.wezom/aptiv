import React from 'react';
import { observer } from 'mobx-react';
import Icon from '~/components/ui/Icon';

@observer
export default class ProductCard extends React.Component {
  render() {
    const {
      item, glyph, width, className,
    } = this.props;
    return (
      <div
        className={`product-inner ${className || ''} c-pointer`}
        data-item={item.id}
        onClick={this.props.onClick}
        role="presentation"
      >
        <div className="product-inner__image">
          <Icon glyph={glyph} style={{ width }} />
        </div>
        <div className="product-inner__title">{item.title}</div>
      </div>
    );
  }
}
