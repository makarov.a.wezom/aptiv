import React from 'react';
import { observer } from 'mobx-react';
import CSSTransition from 'react-transition-group/CSSTransition';
import CSSTransitionGroup from 'react-transition-group/TransitionGroup';
import CatalogItem from '~/components/CatalogItem';

// const duration = 300;

/* const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  opacity: 0,
};

const transitionStyles = {
  entering: { opacity: 0 },
  entered: { opacity: 1 },
}; */

@observer
export default class CatalogItemsList extends React.Component {
  componentDidMount() {
    // , items: []
    // this.state.items = this.props.items.slice();
    /* setInterval(() => {
        this.setState(prevState => ({
          items: prevState.items.slice(0, Math.ceil(this.state.items.length / 2)),
        }));
      }, 3000); */
  }

  render() {
    const items = this.props.items.map((item, index) => (
      <CSSTransition
        mountOnEnter
        unmountOnExit
        key={`${this.props.namespace}.${this.props.namespace}.${item.id}`}
        timeout={400}
        classNames="example"
      >
        <CatalogItem
          namespace={this.props.namespace}
          key={`${this.props.namespace}.${this.props.namespace}.${item.id}`}
          index={index}
          className="gcell gcell--12 gcell--md-6 gcell--lg-4"
          item={item}
          onVote={this.props.onVote}
        />
      </CSSTransition>
    ));

    return (
      <CSSTransitionGroup component="div" className={`grid grid--space-def grid--vspace-def ${this.props.hide ? '_hide' : ''}`}>
        {items}
      </CSSTransitionGroup>
    );
  }
}
