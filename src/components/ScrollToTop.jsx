import React from 'react';
import { withRouter } from 'react-router';

@withRouter
export default class ScrollToTopOnMount extends React.Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    // console.log('scroll to top on mount'); // eslint-disable-line
    return this.props.children;
  }
}
