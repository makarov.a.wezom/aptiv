import React from 'react';
import { observer, inject } from 'mobx-react';
import { observable, action } from 'mobx';
import { withRouter } from 'react-router-dom';
import Icon from '~/components/ui/Icon';
import Tooltip from '~/components/ui/Tooltip';
import ArrowRightIcon from '~/style/svg/arrow-right.svg';
import PreloaderIcon from '~/style/svg/preloader.svg';
import { NO_IMAGE_PATH } from '~/config/config';

@withRouter
@inject('catalogStore', 'modalsStore', 'routing')
@observer
export default class CatalogItem extends React.Component {
  @observable votePending = false;
  @observable disableVoting = false;
  @action vote = () => {
    const { match: { params } } = this.props;
    this.props.modalsStore.setVoteModalParams({
      categoryId: params.categoryId,
      productId: params.productId,
      item: this.props.item,
    });
    this.props.modalsStore.openModal('submitVote');
  };
  gotoInner = () => {
    if (this.props.item.statusId === 3) return;
    const { match: { params } } = this.props;
    if (this.props.namespace === 'search') {
      this.props.history.push(`/product-lines/products/${this.props.item.id}`);
    } else if (params.categoryId) {
      this.props.history.push(`/product-lines/${params.categoryId}/products/${this.props.item.id}`);
    } else {
      this.props.history.push(`/product-lines/${this.props.item.id}`);
    }
  };
  handleVote = () => {
    if (this.props.item.statusId === 3 && !this.disableVoting) {
      this.vote();
    }
  };
  checkBg = () => this.props.item.backgroundImageThumbnail !== NO_IMAGE_PATH;
  checkImg = () => this.props.item.mainImageThumbnail !== NO_IMAGE_PATH;
  render() {
    const { item, className } = this.props;
    const index = this.props.index + 1;
    return (
      <div
        className={`catalog-item ${className} ${item.statusId === 3 ? 'catalog-item--visible-and-not-active' : ''} `}
      >
        <div
          className={`catalog-item__inner _pl-3 _pr-3 ${item.statusId === 3 ? 'c-pointer' : ''}`}
          style={this.checkBg() ? { backgroundImage: `url(${item.backgroundImageThumbnail})` } : {}}
          onClick={this.handleVote}
          disabled={this.disableVoting}
          role="presentation"
        >
          <div
            onClick={this.gotoInner}
            className={`catalog-item__image ${item.statusId !== 3 ? 'c-pointer' : ''}`}
            style={{ backgroundImage: `url(${this.checkImg() ? item.mainImageThumbnail : ''})` }}
            role="presentation"
          />
          <div className="catalog-item__number">
            {index > 9 ? index : `0${index}`}
          </div>
          <div className="catalog-item__title">
            <a className={item.statusId !== 3 ? 'c-pointer' : ''} onClick={this.gotoInner} role="presentation">{item.name}</a>
          </div>
          <div className="catalog-item__link _pb-4">
            {
                item.statusId === 3 ?
                    (
                      <div
                        className={`catalog-item__action-text ${this.disableVoting ? 'catalog-item__action-text--disabled' : 'c-pointer'}`}
                      >
                        <span className="_flex _items-center">
                          <span className="_iblock catalog-item__link-text">Vote</span>
                          {
                            !this.votePending ? (
                              <Tooltip
                                element={(<span className="round-icon _ml-1 _iblock">?</span>)}
                                tooltipContent="By voting, you can let our team know about your interest in this product"
                              />
                            ) : (
                              <span className="_ml-1 icon icon--md">
                                <Icon glyph={PreloaderIcon} style={{ width: '1rem' }} />
                              </span>
                            )
                          }
                        </span>
                      </div>
                    ) :
                    (
                      <a
                        role="presentation"
                        className="catalog-item__action-text c-pointer"
                        onClick={this.gotoInner}
                      >
                        <span className="catalog-item__text">browse</span>
                        <span className="catalog-item__right-icon _pl-1">
                          <Icon glyph={ArrowRightIcon} style={{ width: '2.8125rem' }} />
                        </span>
                      </a>
                    )
            }
          </div>
        </div>
      </div>
    );
  }
}
