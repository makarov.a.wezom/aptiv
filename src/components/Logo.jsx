import React from 'react';
import { inject, observer } from 'mobx-react';
import { Link, withRouter } from 'react-router-dom';
import LogoImage from '~/style/pic/logo.png';

@withRouter
@inject('routing')
@observer
export default class Logo extends React.Component {
  render() {
    return this.props.routing.location.pathname !== '/' ? (
      <Link to="/" className={`header__logo _p-1 opacity-on-hover _flex _items-center ${this.props.className || ''}`}>
        <img src={LogoImage} alt="" />
      </Link>
    ) :
      (
        <span className={`header__logo _flex _items-center ${this.props.className || ''}`}>
          <img src={LogoImage} alt="" />
        </span>
      );
  }
}
