import React from 'react';
import { observer } from 'mobx-react';

export default observer(({
  field, type = 'text', placeholder = null, className,
}) => (
  <div className={`form-group _mb-4 ${className}`}>
    <label
      htmlFor={field.id}
      className="_mb-1 _color-gray-4 form-label"
    >
      {field.label}
    </label>
    <input
      className="_d-block _w-100 form-field"
      {...field.bind({ type, placeholder })}
    />
    <small className="_color-danger">
      {field.error}
    </small>
  </div>
));
