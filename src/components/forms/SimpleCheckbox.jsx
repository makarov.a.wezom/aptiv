import React from 'react';
import { observer } from 'mobx-react';

export default observer(({ field }) => (
  <div className="form-group">
    <label className={`form-checkbox htmlFor=${field.id}`}>
      <input className="_hide form-checkbox__input" {...field.bind()} />
      <span className="form-checkbox__square" />
      <span>{field.label}</span>
    </label>
    {
        field.error &&
        (
        <div>
          <small className="_color-danger">The Enter e-mail field is required.</small>
        </div>
        /* <div>
          <label className="has-error" htmlFor="checkbox" id="checkbox-error" style={{ display: 'none' }} />
        </div> */
        )
    }
  </div>
));

