import React from 'react';
import { observer } from 'mobx-react';

@observer
export default class PreventClicks extends React.Component {
    handlePreventClick = (e) => {
      e.preventDefault();
      e.stopPropagation();
    };
    /* TODO: upgrade to react 16+ with Fragment */
    render() {
      return (
        <div role="presentation" onClick={this.handlePreventClick} style={{ ...this.props.style }}>
          {this.props.children}
        </div>
      );
    }
}
