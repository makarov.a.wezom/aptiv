import React from 'react';
import { observer } from 'mobx-react';
import Modal from 'react-modal';
import Icon from '~/components/ui/Icon';
import CrossIcon from '~/style/svg/cross.svg';

export default observer(({
  isOpen, onAfterOpen, onCloseModal: closeModal, title, content, className,
}) =>
  (
    <Modal
      isOpen={isOpen}
      onAfterOpen={onAfterOpen}
      onRequestClose={closeModal}
      className={`react-modal__modal ${className || ''}`}
      overlayClassName="react-modal__overlay"
      closeTimeoutMS={200}
    >
      <div className="_mt-0 _mb-5 form-caption _text-center _color-black">
        <span>{title}</span>
        <div className="close-modal" onClick={closeModal} role="presentation">
          <Icon
            glyph={CrossIcon}
            style={{ width: '1.125rem' }}
          />
        </div>
      </div>
      {content}
    </Modal>
  ));
