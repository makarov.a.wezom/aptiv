import React from 'react';
import { observer } from 'mobx-react';

export default observer(({
  disabled = false,
  content = null,
  type = 'button',
  className,
  onClick,
  text,
  iconStart,
  iconEnd,
}) => (
  <button
    type={type}
    disabled={disabled}
    onClick={onClick}
    className={className}
  >
    {content ||
    <span>
      {iconStart}
      {<span className="text">{text}</span>}
      {iconEnd}
    </span>}
  </button>
));
