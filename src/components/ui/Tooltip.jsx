import React from 'react';
import { observer } from 'mobx-react';
import PreventClicks from '~/components/mixins/PreventClicks';

/* eslint-disable */
export default observer(({
    element,
    tooltipContent,
    direction,
    enableClicks,
    className,
    style,
}) => {
    /*
    const TooltipEl =
        (
            <div className={`${className} tooltip rel _fs-0`}>
        {element}
        <div className={`tooltip__container tooltip__container--${direction || 'right'}`}>
            {tooltipContent}
        </div>
    </div>
    );
        */
    return !enableClicks ? (
        <PreventClicks style={{...style}}>
            <div className={`${className || ''} tooltip rel _fs-0`}>
                {element}
                <div className={`tooltip__container tooltip__container--${direction || 'right'}`}>
                    {tooltipContent}
                </div>
            </div>
        </PreventClicks>
    ) : (
        <div className={`${className || ''} tooltip rel _fs-0`} style={{...style}}>
            {element}
            <div className={`tooltip__container tooltip__container--${direction || 'right'}`}>
                {tooltipContent}
            </div>
        </div>
    )
});
