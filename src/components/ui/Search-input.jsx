import React from 'react';
import { inject, observer } from 'mobx-react';
import Icon from '~/components/ui/Icon';
import Preloader from '~/components/ui/Preloader';
import SearchIcon from '~/style/svg/search.svg';
import { withRouter } from 'react-router-dom';

@withRouter
@inject('catalogStore')
@observer
export default class SearchInput extends React.Component {
    handleChange = (event) => {
      this.props.catalogStore.updateSearchValue(event.target.value);
    };
    handleSearch = () => {
      this.props.catalogStore.startSearch(this.props.catalogStore.search, this.props.history);
    };
    handleKeypress = (e) => {
      if (e.charCode === 13) {
        this.props.catalogStore.startSearch(this.props.catalogStore.search, this.props.history);
      }
    };
    handleSubmit = (e) => {
      e.preventDefault();
      e.stopPropagation();
      return false;
    };

    render() {
      return (
        <form className="searchInput" onSubmit={this.handleSubmit}>
          <input
            type="text"
            value={this.props.catalogStore.search}
            disabled={this.props.catalogStore.searchPending}
            onChange={this.handleChange}
            className="searchInput__input"
            onKeyPress={this.handleKeypress}
            placeholder="SEARCH PRODUCT (i.e. APEX, CTCS, FAKRA)"
          />
          {
              !this.props.catalogStore.searchPending ?
              (
                <button className="searchInput__ico searchInput__ico--search c-pointer opacity-on-hover-inverse" onClick={this.handleSearch}>
                  <Icon className="icon icon--xs" glyph={SearchIcon} />
                </button>
              ) :
              (
                <div className="searchInput__ico">
                  <Preloader />
                </div>
              )
          }
        </form>
      );
    }
}
