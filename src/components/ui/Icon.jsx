import React from 'react';

export default ({ glyph, options = { width: '100%', height: '100%' }, ...rest }) => (
  <svg viewBox={glyph.viewBox} xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" {...options} {...rest} >
    <use
      xlinkHref={`#${glyph.id}`}
    />
  </svg>
);
