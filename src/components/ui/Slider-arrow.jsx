import React from 'react';
import Icon from '~/components/ui/Icon';
import LeftIcon from '~/style/svg/left.svg';

export default ({
  className, style, onClick, additionalClassName,
}) => (
  <div
    role="presentation"
    className={`${className} ${additionalClassName}`}
    style={{ ...style, display: 'block' }}
    onClick={onClick}
  >
    <Icon glyph={LeftIcon} />
  </div>
);
