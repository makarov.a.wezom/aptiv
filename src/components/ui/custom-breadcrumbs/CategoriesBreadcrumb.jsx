import React from 'react';
// import { get } from 'mobx';
import { observer, inject } from 'mobx-react';

@inject('catalogStore')
@observer
export default class CategoriesBreadcrumb extends React.Component {
  render() {
    const { match: { params } } = this.props;
    const category = this.props.catalogStore.categoriesItemsById ? this.props.catalogStore.categoriesItemsById[params.categoryId] : false;
    if (category) {
      return (<span>{category.name}</span>);
    }
    return (<span>{params.categoryId}</span>);
  }
}
