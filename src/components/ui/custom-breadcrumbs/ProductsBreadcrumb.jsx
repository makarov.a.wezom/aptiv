import React from 'react';
import { observer, inject } from 'mobx-react';

@inject('catalogStore')
@observer
export default class ProductsBreadcrumb extends React.Component {
  render() {
    const { match: { params } } = this.props;
    const product = this.props.catalogStore.productsItemsById[params.productId];
    if (product) {
      return (<span>{product.name}</span>);
    }
    return (<span>{params.productId}</span>);
  }
}
