import React from 'react';
import { observer } from 'mobx-react';
import Icon from '~/components/ui/Icon';
import SquareIcon from '~/style/svg/square.svg';

@observer
export default class FullScreenIcon extends React.Component {
  render() {
    const {
      className, style, CustomIcon,
    } = this.props;
    return (
      <div
        role="presentation"
        className={`full-screen-icon opacity-on-hover c-pointer ${className || ''}`}
        style={style}
        onClick={this.props.onClick}
      >
        <Icon glyph={SquareIcon || CustomIcon} />
      </div>
    );
  }
}
