import React from 'react';
// import { inject, observer } from 'mobx-react/index';
import { observer } from 'mobx-react';
import { NavLink } from 'react-router-dom';
import withBreadcrumbs from 'react-router-breadcrumbs-hoc';

import CategoriesBreadcrumb from '~/components/ui/custom-breadcrumbs/CategoriesBreadcrumb';
import ProductsBreadcrumb from '~/components/ui/custom-breadcrumbs/ProductsBreadcrumb';

@observer
class Breadcrumbs extends React.Component {
  render() {
    const { breadcrumbs } = this.props;
    const ln = breadcrumbs.length - 1;
    return (
      <div className="breadcrumbs _pb-4">
        {
                    breadcrumbs.map((breadcrumb, index) => (ln !== index ?
                            (
                              <span key={breadcrumb.key}>
                                <NavLink to={breadcrumb.props.match.url}>
                                  {breadcrumb}
                                </NavLink>
                              </span>
                            ) :
                            (<span key={breadcrumb.key} className="_color-primary">{breadcrumb}</span>)))
                }
      </div>
    );
  }
}

const routes = [
  { path: '/product-lines/:categoryId([0-9]+)', breadcrumb: CategoriesBreadcrumb },
  { path: '/product-lines/:categoryId([0-9]+)/products/:productId([0-9]+)', breadcrumb: ProductsBreadcrumb },
  { path: '/product-lines/:categoryId([0-9]+)/products', breadcrumb: null },
  { path: '/product-lines/', breadcrumb: (<span>Product lines</span>) },
  { path: '/product-lines/products', breadcrumb: null },
  { path: '/product-lines/products/:productId([0-9]+)', breadcrumb: ProductsBreadcrumb },
  { path: '/', breadcrumb: null },
];

const options = { excludePaths: ['/product-lines/products', '/product-lines/:categoryId([0-9]+)/products', '/product-lines/:categoryId([0-9]+)/products/'] };
export default withBreadcrumbs(routes, options)(Breadcrumbs);
