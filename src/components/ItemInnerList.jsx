import React from 'react';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import ProductCard from '~/components/ProductCard';
import PictureIcon from '~/style/svg/picture.svg';
import FilesIcon from '~/style/svg/files.svg';
import PlayIcon from '~/style/svg/play.svg';
import LayersIcon from '~/style/svg/layers.svg';
import StandIcon from '~/style/svg/stand.svg';

@withRouter
@inject('routing')
@observer
export default class ItemInnerList extends React.Component {
  goto = (path) => {
    const { match: { params } } = this.props;
    this.props.routing.push(`${`/product-lines${params.categoryId ? `/${params.categoryId}` : ''}`}/products/${params.productId}/${path}`);
  };
  render() {
    const {
      images = [], datasheets = [], presentations = [], catalogues = [], brochures = [],
    } = this.props.item;
    return (
      <div className="product-inner-list">
        {!(
                  images.length ||
                  datasheets.length ||
                  presentations.length ||
                  catalogues.length ||
                  brochures.length
              ) ?
                <h1>No data for this record</h1> : ''
          }
        <div className="grid grid--space-def grid--vspace-def _justify-center">
          {
              images.length ? (
                <div className="gcell gcell--12 gcell--xs-6 gcell--md-4 gcell--xl-4">
                  <ProductCard
                    onClick={() => this.goto('images')}
                    glyph={PictureIcon}
                    width="3.6875rem"
                    item={{ title: 'Images' }}
                  />
                </div>
              ) : ''
          }
          {
            datasheets.length ? (
              <div className="gcell gcell--12 gcell--xs-6 gcell--md-4 gcell--xl-4">
                <ProductCard
                  onClick={() => this.goto('datasheets')}
                  glyph={FilesIcon}
                  width="3.6875rem"
                  item={{ title: 'Datasheets' }}
                />
              </div>
            ) : ''
          }
          {
              presentations.length ? (
                <div className="gcell gcell--12 gcell--xs-6 gcell--md-4 gcell--xl-4">
                  <ProductCard
                    onClick={() => this.goto('presentations')}
                    glyph={PlayIcon}
                    width="3.6875rem"
                    item={{ title: 'Presentation' }}
                  />
                </div>
              ) : ''
          }
          {
                catalogues.length ? (
                  <div className="gcell gcell--12 gcell--xs-6 gcell--md-4 gcell--xl-4">
                    <ProductCard
                      onClick={() => this.goto('catalogues')}
                      glyph={LayersIcon}
                      width="3.6875rem"
                      item={{ title: 'Catalogue' }}
                    />
                  </div>
              ) : ''
          }
          {
              brochures.length ? (
                <div className="gcell gcell--12 gcell--xs-6 gcell--md-4 gcell--xl-4">
                  <ProductCard
                    onClick={() => this.goto('brochures')}
                    glyph={StandIcon}
                    width="3.6875rem"
                    item={{ title: 'Brochure' }}
                  />
                </div>
              ) : ''
          }
        </div>
      </div>
    );
  }
}
