import React from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import Icon from '~/components/ui/Icon';
import DescIcon from '~/style/svg/desc.svg';

@observer
export default class CustomFullScreen extends React.Component {
  constructor() {
    super();
    if (
      document.body.requestFullscreen ||
            document.body.mozRequestFullScreen ||
            document.body.webkitRequestFullscreen ||
            document.body.msRequestFullscreen
    ) {
      this.supportsFullscreen = true;
    }
  }
    @observable supportsFullscreen = false;
    render() {
      return (
        <button
          className="button button--without-bd button--rounded button--only-icon c-pointer"
          onClick={this.props.onClick}
        >
          <span><Icon glyph={DescIcon} style={{ width: '2.2rem', height: '2.2rem' }} /></span>
        </button>
      );
    }
}

/*
this.supportsFullscreen ? (

) : (
    <a
        className="button button--without-bd button--rounded button--only-icon c-pointer"
        href={this.props.link}
        target="_blank" // eslint-disable-line react/jsx-no-target-blank
    >
        <span><Icon glyph={DescIcon} style={{ width: '2.2rem', height: '2.2rem' }} /></span>
    </a>
) */
