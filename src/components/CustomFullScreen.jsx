import React from 'react';
import Fullscreen from 'react-full-screen';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { observable } from 'mobx';

@withRouter
@inject('catalogStore', 'modalsStore')
@observer
export default class CustomFullScreen extends React.Component {
  constructor() {
    super();
    if (
      document.body.requestFullscreen ||
            document.body.mozRequestFullScreen ||
            document.body.webkitRequestFullscreen ||
            document.body.msRequestFullscreen
    ) {
      this.supportsFullscreen = true;
    }
  }
    @observable supportsFullscreen = false;
    render() {
      return (
        <Fullscreen
          enabled={this.props.enabled}
          onChange={this.props.onChange}
        >
          {this.props.enabled ? (
            <div
              className={`fullscreen__wrapper ${!this.supportsFullscreen ? 'fullscreen__wrapper--manual' : ''}`}
              style={!this.props.enabled ? { display: 'none' } : {}}
            >
              {
                          !this.supportsFullscreen ?
                              (
                                <div className="fullscreen__closer _flex _justify-end _pb-2">
                                  <div
                                    role="presentation"
                                    className="button button--primary-o button--border-gray closeFullscreen"
                                    onClick={this.props.onCloseClick}
                                  >
                                    <span>close</span>
                                  </div>
                                </div>
                              ) : ''
                      }
              {this.props.children}
            </div>
              ) : ''}
        </Fullscreen>
      );
    }
}

