import '~/config/polyfills';
/* eslint-disable */
import '~/style/styles/style.scss';
import React from 'react';
import ReactDOM from 'react-dom';

// import DevTools from 'mobx-react-devtools';
// import MobxReactFormDevTools from 'mobx-react-form-devtools';
// import MobxReactForm from 'mobx-react-form'; // eslint-disable-line
// register forms
/*
if (process.env.NODE_ENV !== 'production') {
    MobxReactFormDevTools.register({
        loginForm: LoginFormScheme,
        privacyForm: PrivacyFormScheme,
    });
    MobxReactFormDevTools.select('loginForm');
    // MobxReactFormDevTools.open(true);
}

import LoginFormScheme from '~/containers/forms/LoginFormScheme';
import PrivacyFormScheme from '~/containers/forms/PrivacyFormScheme';
*/

import { Router } from 'react-router';
import { Provider } from 'mobx-react';
import createBrowserHistory from 'history/createBrowserHistory';
import { syncHistoryWithStore } from 'mobx-react-router';
import ScrollToTop from '~/components/ScrollToTop';
import Modal from 'react-modal';
import App from '~/containers/App';
import stores from '~/stores';
import routing from '~/stores/routing';

/* eslint-enable */

const browserHistory = createBrowserHistory();
const history = syncHistoryWithStore(browserHistory, routing);

window._____APP_STATE_____ = stores; // eslint-disable-line

const render = (Component) => {
  const el = document.querySelector('#root');
  Modal.setAppElement('#root');
  ReactDOM.render(
    (
      <Provider {...stores}>
        <Router history={history}>
          <ScrollToTop>
            <Component />
          </ScrollToTop>
        </Router>
      </Provider>
    ),
    el,
  );
};

/* {
    process.env.NODE_ENV !== 'production' &&
    (
        <span>
                  <DevTools key={1} />
                  <MobxReactFormDevTools.UI key={2} />
                </span>
    )
} */

render(App);

/*
let deferredPrompt;

window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  deferredPrompt = e;

  deferredPrompt.prompt();
  // Wait for the user to respond to the prompt
  deferredPrompt.userChoice
    .then((choiceResult) => {
      if (choiceResult.outcome === 'accepted') {
        console.log('User accepted the A2HS prompt'); // eslint-disable-line no-console
      } else {
        console.log('User dismissed the A2HS prompt');// eslint-disable-line no-console
      }
      deferredPrompt = null;
    });
});
*/
