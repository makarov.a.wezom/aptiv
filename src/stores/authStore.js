import { observable, action } from 'mobx';
import api from '~/api/api';
import commonStore from '~/stores/commonStore';
import modalsStore from '~/stores/modalsStore';
import catalogStore from '~/stores/catalogStore';
import toastrStore from '~/stores/toastrStore';
import routing from '~/stores/routing';
import { sendMessageToSw } from '../helpers/sw-messaging';
import { API_PATH } from '../config/config';

class AuthStore {
    @observable loggedIn = false;
    @observable pending = false;
    @observable privacyPending = false;
    @observable errors = undefined;
    @observable checkAuthProgress = false;
    @observable checkAuthError = false;
    @observable privacyError = false;
    @observable requireAuth = true;
    @observable meta = {};

    @observable values = {
      username: '',
    };

    @action setUsername(username) {
      this.values.username = username;
    }

    @action reset() {
      this.values.username = '';
      this.pending = false;
      this.errors = undefined;
    }

    @action clearLogout() {
      this.loggedIn = false;
    }

    @action login(values) {
      this.reset();
      this.pending = true;

      return api.Auth.login(values)
        .then(({ data }) => {
          this.setUsername(values.username);
          commonStore.setToken(data);
          modalsStore.closeModal();
          routing.push('product-lines');
          this.loggedIn = true;
        })
        .catch(action((err) => {
          this.errors = err.response && err.response.body && err.response.body.errors;
          throw err;
        }))
        .finally(action(() => {
          this.pending = false;
        }));
    }

    @action logout() {
      this.logoutPending = true;
      this.logoutError = false;
      return api.Auth.logout().then(() => {
        this.loggedIn = false;
        catalogStore.clearData();
        commonStore.setToken({});
        this.reset();
        modalsStore.closeModal();
        routing.push('/');
        sendMessageToSw(JSON.stringify({
          data: [
            `${API_PATH}api/auth/check`,
          ],
          type: 'CLEAR_CACHE',
        }));
      }, (error) => {
        this.logoutError = error;
        toastrStore.container.error('Error during logout...');
      }).finally(() => {
        this.logoutPending = true;
      });
    }

    @action checkAuth() {
      this.checkAuthProgress = true;
      this.checkAuthError = false;
      return Promise
        .all([api.Auth.checkAuth(), api.Auth.getPolicyText()])
        .then((responses) => {
          const { data: authData = {} } = responses[0];
          const { data: policyData = {} } = responses[1];
          this.requireAuth = authData.require;
          this.meta = policyData.meta || {
            policy: '',
          };
          modalsStore.openModal('privacy');
        }, (errors) => {
          this.checkAuthError = errors[0]; // eslint-disable-line prefer-destructuring
        }).finally(() => {
          this.checkAuthProgress = false;
        });
    }

    @action checkRequiredAuth() {
      this.checkAuthProgress = true;
      this.checkAuthError = false;
      return api.Auth.checkAuth().then(({ data = {} }) => {
        this.requireAuth = data.require;
        this.meta = data.meta || {
          policy: '',
        };
      }, (error) => {
        this.checkAuthError = error;
      }).finally(() => {
        this.checkAuthProgress = false;
      });
    }

    @action acceptPrivacy() {
      this.privacyPending = true;
      this.privacyError = false;
      return api.Auth.acceptPrivacy().then(({ data }) => {
        this.requireAuth = data.required;
        if (data.success) {
          if (this.requireAuth) {
            modalsStore.openModal('login');
          } else {
            modalsStore.closeModal();
            routing.push('product-lines');
          }
        }
      }, (error) => {
        this.privacyError = error;
      }).finally(() => {
        this.privacyPending = false;
      });
    }
}

export default new AuthStore();
