import { observable, action } from 'mobx';
// import { observer } from 'mobx-react';
import api from '~/api/api';
import toastrStore from '~/stores/toastrStore';
import modalsStore from '~/stores/modalsStore';
import routing from '~/stores/routing';
import parseQuery from '../helpers/parse-url-query';
import { sendMessageToSw } from '../helpers/sw-messaging';

// import {DEFAULT_ERROR_MESSAGE} from "~/config/config";

class CatalogStore {
    @observable categoriesLoaded = false;
    @observable categoriesPending = false;
    @observable categoriesError = false;
    @observable categoriesItems = [];
    @observable categoriesItemsById = {};
    @observable categoriesVotePending = false;
    @observable categoriesVoteError = false;
    @observable productsLoaded = false;
    @observable productsPending = false;
    @observable productsError = false;
    @observable productsItems = [];
    @observable productsItemsById = {};
    @observable productsVotePending = false;
    @observable productsVoteError = false;
    @observable emailPending = false;
    @observable emailError;
    @observable search = parseQuery(routing.history.location.search).text || '';
    @observable searchPending = false;
    @observable searchError = false;
    @observable searchItems = [];
    @observable searchItemsById = {};
    @observable meta = {
      scheduleFileLink: '',
    };

    @action fetchRequest(namespace, request) {
      this.startFetch(namespace);
      return request().then((response) => {
        this.fetchSuccess(namespace);
        return Promise.resolve(response);
      }, (error) => {
        this.fetchFail(namespace);
        return Promise.reject(error);
      });
    }

    @action startFetch(namespace) {
      this[`${namespace}Pending`] = true;
      this[`${namespace}Error`] = false;
    }

    @action fetchSuccess(namespace) {
      this[`${namespace}Pending`] = false;
      if (typeof this[`${namespace}Loaded`] !== 'undefined') {
        this[`${namespace}Loaded`] = true;
      }
    }

    @action fetchFail(namespace, error) {
      this[`${namespace}Pending`] = false;
      this[`${namespace}Error`] = error;
    }

    @action fetchItems() {
      this.categoriesPending = true;
      return this.fetchRequest('categories', api.Catalog.groupsCache).then(({ data }) => {
        this.categoriesItems.replace(data.data || []);
        this.meta = data.meta || {};
        this.categoriesItemsById = this.categoriesItems.slice().reduce((accumulator, currentValue) => {
                accumulator[currentValue.id] = currentValue; // eslint-disable-line
          return accumulator;
        }, this.categoriesItemsById);
        this.productsItemsById = Object.keys(this.categoriesItemsById).reduce((accumulator, currentValue) => {
          this.categoriesItemsById[currentValue].products.forEach((product) => {
            accumulator[product.id] = product; // eslint-disable-line
          });
          // accumulator[currentValue].products.map(item => item);
          return accumulator;
        }, {});
        this.productsItems.replace(Object.values(this.productsItemsById));
        this.categoriesLoaded = true;
        this.categoriesPending = false;
        const imagesNamespace = ['backgroundImageOriginal', 'backgroundImageThumbnail', 'mainImageOriginal', 'mainImageThumbnail'];
        const innerNamespaces = ['datasheets', 'brochures', 'catalogues', 'presentations', 'images'];
        const imagesInnerNamespace = ['urlOriginal', 'urlThumbnail', 'urlThumbnailSlider'];
        const imagesArray = this.categoriesItems.slice().reduce((acc, cur) => {
          imagesNamespace.forEach(imgName => acc.push(cur[imgName]));
          return acc.concat(cur.products.reduce((prAcc, prCur) => {
            imagesNamespace.forEach(imgName => prAcc.push(prCur[imgName]));
            innerNamespaces.forEach(innerNamespace => prCur[innerNamespace].forEach((ns) => {
              imagesInnerNamespace.forEach(innerImage => prAcc.push(ns[innerImage]));
            }));
            return prAcc;
          }, []));
        }, []);

        sendMessageToSw(JSON.stringify({
          categoriesItems: imagesArray,
          type: 'PRECACHE_ITEMS',
        }));
      }, (err) => {
        this.categoriesPending = false;
        return Promise.reject(err);
      });
    }

    @action updateSearchValue(search) {
      this.search = search;
    }

    @action startSearch(params, history) {
      history.push(`/product-lines/search?text=${params || ''}`);
      this.fetchSearch(params);
    }

    @action applySearch(searchText) {
      this.searchItems.replace(this.productsItems.slice().filter(item => ~item.name.toLowerCase().indexOf(searchText.toLowerCase())));
    }

    @action fetchSearch(searchText = '') {
      this.startFetch('search');
      if (!this.categoriesLoaded) {
        this.searchItems.replace([]);
        this.fetchItems().then(() => {
          this.applySearch(searchText);
        }, () => {
          this.fetchFail('search');
        }).finally(() => {
          this.searchPending = false;
        });
      } else {
        this.applySearch(searchText);
        this.searchPending = false;
      }
    }

    @action categoriesVote = item => this.fetchRequest('categoriesVote', () => api.Catalog.categoriesVote(item.id)).then(() => {
      toastrStore.container.success('Your vote has been accepted');
      // this.fetchItems();
    });

    @action productsVote = item => this.fetchRequest('productsVote', () => api.Catalog.productsVote(item.id)).then(() => {
      toastrStore.container.success('Your vote has been accepted');
      // this.fetchItems();
    });

    @action sendToEmail(formParams) {
      const { productId, ...params } = formParams;
      return this.fetchRequest('email', () => api.Catalog.sendToEmail(productId, params)).then(() => {
        toastrStore.container.success(`Data has been sent successfully to: ${formParams.emailTo}`);
        modalsStore.closeModal();
      });
    }

    @action clearData() {
      this.search = '';
      this.categoriesItems.replace([]);
      this.categoriesItemsById = {};
      this.productsItems.replace([]);
      this.productsItemsById = {};
    }
}

const catalogStore = new CatalogStore();

export default catalogStore;
