import { observable, action } from 'mobx';
// import api from '~/api/api';

// const jwt = require('jsonwebtoken');

class UserStore {
    @observable currentUser;
    @observable loadingUser;
    @observable updatingUser;
    @observable updatingUserErrors;

    @action pullUser(token) {
      this.loadingUser = true;
      if (token) {
        this.currentUser = window.localStorage.getItem('user');
      }
      this.loadingUser = false;
      /* eslint-disable */
        // const decoded = jwt.decode(token, { complete: true });

        /* return api.Auth.current()
          .then(action(({ user }) => { this.currentUser = user; }))
          .finally(action(() => { this.loadingUser = false; })); */
        /* eslint-enable */
    }

    @action updateUser(token) {
      this.updatingUser = true;
      if (token) {
        this.currentUser = window.localStorage.getItem('user');
      }
      this.updatingUser = false;
    }

    @action forgetUser() {
      this.currentUser = undefined;
    }
}

export default new UserStore();
