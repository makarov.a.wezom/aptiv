import authStore from '~/stores/authStore';
import commonStore from '~/stores/commonStore';
import userStore from '~/stores/userStore';
import modalsStore from '~/stores/modalsStore';
import catalogStore from '~/stores/catalogStore';
import toastrStore from '~/stores/toastrStore';
import routing from '~/stores/routing';

const stores = {
  authStore,
  commonStore,
  userStore,
  modalsStore,
  catalogStore,
  toastrStore,
  routing,
};

export default stores;
