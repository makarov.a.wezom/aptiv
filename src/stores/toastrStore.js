import { observable, action } from 'mobx';

class ToastrStore {
    @observable.ref container;
    @action setContainer = (ref) => {
      this.container = ref;
    }
}

export default new ToastrStore();
