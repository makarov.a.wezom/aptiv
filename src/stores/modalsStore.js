import { observable, action } from 'mobx';
// import { BrowserRouter } from 'react-router-dom';
// import authStore from '~/stores/authStore';

// @withRouter
class ModalsStore {
    @observable loadingUser;
    @observable openedModal = '';
    @observable voteModalParams = {
      productId: undefined, categoryId: undefined, namespace: '', item: {},
    };

    @action openModal(modalType) {
      this.openedModal = modalType;
    }

    @action setVoteModalParams(voteModalParams) {
      this.voteModalParams = voteModalParams;
    }

    @action closeModal() {
      this.openedModal = '';
    }
}

const modalsStore = new ModalsStore();

export default modalsStore;
