import { observable, action, reaction } from 'mobx';

class CommonStore {
    @observable appName = 'Aptiv';
    @observable token = window.localStorage.getItem('jwt') ? JSON.parse(window.localStorage.getItem('jwt')) : {};
    @observable appLoaded = false;

    constructor() {
      reaction(
        () => this.token,
        (token) => {
          if (token) {
            window.localStorage.setItem('jwt', JSON.stringify(token));
          } else {
            window.localStorage.removeItem('jwt');
          }
        },
      );
      reaction(
        () => this.appLoaded,
        (appLoaded) => {
          if (appLoaded) {
            this.hideSitePreloader();
          }
        },
      );
    }

    @action setToken({ access_token: accessToken, refresh_token: refreshToken }) {
      this.token = {
        accessToken,
        refreshToken,
      };
    }

    @action setAppLoaded() {
      this.appLoaded = true;
    }

    hideSitePreloader() {
      /* while no js / css there is a preloader */
      document.querySelector('.site-preloader').classList.add('site-preloader--hidden');
    }
}

export default new CommonStore();
