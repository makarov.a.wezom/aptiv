// const moment = require('moment');

export const DATE_FORMAT = 'DD.MM.YYYY';
export const DEFAULT_ERROR_MESSAGE = 'Ups, something went wrong.';
export const NO_IMAGE_PATH = '/media/pic/no-image-default.png';
export const NO_AVATAR_PATH = '/media/pic/no-avatar-default.png';

// process.env.NODE_ENV

export const API_PATH = '//aptiv-pwa.azurewebsites.net/';

// moment.defaultFormat = DATE_FORMAT;
