import axios from 'axios';
import { API_PATH, DEFAULT_ERROR_MESSAGE } from '~/config/config';
import toastrStore from '~/stores/toastrStore';
import commonStore from '~/stores/commonStore';
import authStore from '~/stores/authStore';
import routing from '~/stores/routing';
import api from '~/api/api';

require('promise.prototype.finally');
// import store from 'store/configureStore';

const axiosInstance = axios.create({
  baseURL: API_PATH,
  responseType: 'json',
  /* withCredentials: true */
});

axiosInstance.interceptors.request.use((config) => {
  /* TODO: auth interceptors */
  const patchedConfig = {
    ...config,
  };
  if (commonStore.token && commonStore.token.accessToken) {
    patchedConfig.headers = {
      ...patchedConfig.headers,
      authorization: `bearer ${commonStore.token.accessToken}`,
    };
  }
  return patchedConfig;
}, error => Promise.reject(error));

axiosInstance.interceptors.response.use((response) => {
  if (response.data && response.data.offline && response.data.message && response.data.showMessage) {
    toastrStore.container.success(response.data.message);
  }
  return response;
}, (error) => {
  if (error.response.status === 401) {
    if (commonStore.token.refreshToken) {
      return api.Auth.refreshAuthToken(commonStore.token.refreshToken)
        .then(({ data }) => {
          commonStore.setToken(data);
          error.config.headers.authorization = `bearer ${data.access_token}`; // eslint-disable-line
          return axios.request(error.config);
        }, () => {
          commonStore.setToken({});
          authStore.clearLogout();
          toastrStore.container.error('Authorization has been expired');
          routing.push('/');
          return Promise.reject(error);
        });
    }
    commonStore.setToken({});
    authStore.clearLogout();
    toastrStore.container.error('Not authorized');
    routing.push('/');
    return Promise.reject(error);
  }
  if (error.response.data && error.response.data.offline && error.response.data.message && error.response.data.showMessage) {
    toastrStore.container.error(error.response.data.message);
    return Promise.reject(error);
  }
  toastrStore.container.error(DEFAULT_ERROR_MESSAGE);
  return Promise.reject(error);
});


export default axiosInstance;
