importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.4.1/workbox-sw.js');

// workbox = false;
if (workbox) {
  workbox.core.setLogLevel(workbox.core.LOG_LEVELS.silent);
  const bgSyncPlugin = new workbox.backgroundSync.Plugin('retryQueue', {
    maxRetentionTime: 24 * 60, // Retry for max of 24 Hours
  });

  workbox.routing.registerNavigationRoute('/index.html');
  workbox.precaching.precache(['/index.html', 'https://aptiv-pwa.azurewebsites.net/api/policy']);

  /*
    const exampleImage = 'http://www.loc8tor.com/media/pet-site-home-hero.jpg'
    workbox.precaching.precacheAndRoute([
        {url: exampleImage},
    ]);
    */

  workbox.routing.registerRoute(
    /^https?:\/\/aptiv-pwa\.azurewebsites\.net\//,
    workbox.strategies.staleWhileRevalidate({
      cacheName: 'application-api',
      networkTimeoutSeconds: 6,
      plugins: [
        new workbox.cacheableResponse.Plugin({
          statuses: [0, 200],
          /* headers: {
                      'X-Is-Cacheable': 'true',
                    }, */
        }),
        bgSyncPlugin,
      ],
    }),
  );

  const offlinePostRequests = [
    /^https?:\/\/aptiv-pwa\.azurewebsites\.net\/api\/auth\/check$/,
    /^https?:\/\/aptiv-pwa\.azurewebsites\.net\/api\/logout$/,
    /^https?:\/\/aptiv-pwa\.azurewebsites\.net\/api\/policy$/,
  ];
  /* 500 when trying to login while being offline */
  const offlinePostErrorRequests = [
    /^https?:\/\/aptiv-pwa\.azurewebsites\.net\/token$/,
  ];
  const matcher = (requestsLists = []) => ({ url, event }) => ~requestsLists.findIndex(expr => expr.test(url.href));
  const handler = (handlerParams = {}) => ({ url, event, params }) => {
    const {
      fakeData = {
        meta: {},
        success: true,
        required: false,
        offline: true,
        message: 'Warning! An offline request',
      }, init = { status: 200, statusText: 'Offline!' },
    } = handlerParams;
    if (typeof navigator.onLine !== 'undefined' && !navigator.onLine) {
      const blob = new Blob([JSON.stringify(fakeData, null, 2)], { type: 'application/json' });
      return new Response(blob, init);
    }
    return fetch(event.request)
      .then(response => response.text())
      .then(responseBody => new Response(responseBody));
  };

  const defaultMatcher = matcher(offlinePostRequests);
  const errorsMatcher = matcher(offlinePostErrorRequests);
  const defaultHandler = handler();
  const errorsHandler = handler({
    fakeData: {
      meta: {},
      success: false,
      required: false,
      offline: true,
      showMessage: true,
      message: 'Sorry, you are unable to make this while being offline',
    },
    init: { status: 500, statusText: 'Offline!' },
  });

  workbox.routing.registerRoute(
    errorsMatcher,
    errorsHandler,
    'POST',
  );

  workbox.routing.registerRoute(
    defaultMatcher,
    defaultHandler,
    'POST',
  );

  workbox.routing.registerRoute(
    /.*\.(?:js)/,
    workbox.strategies.staleWhileRevalidate({
      plugins: [bgSyncPlugin],
    }),
  );
  workbox.routing.registerRoute(
    /.*\.css/,
    workbox.strategies.staleWhileRevalidate({
      cacheName: 'css-cache',
      plugins: [bgSyncPlugin],
    }),
  );
  workbox.routing.registerRoute(
    // Cache image files
    /.*\.(?:png|jpg|jpeg|svg|gif)/,
    workbox.strategies.staleWhileRevalidate({
      cacheName: 'image-cache',
      plugins: [bgSyncPlugin],
    }),
  );
  workbox.routing.registerRoute(
    // Cache fonts
    /.*\.(?:otf|ttf|woff|woff2)/,
    workbox.strategies.staleWhileRevalidate({
      cacheName: 'fonts-cache',
      plugins: [bgSyncPlugin],
    }),
  );

  /* workbox.routing.registerRoute(
      // /^(?:(?!sockjs-node\/info).*)$/,
      [/\.html/, /product-lines/],
      workbox.strategies.staleWhileRevalidate(),
    ); */

  self.addEventListener('message', (event) => {
    let eventData = {};
    try {
      eventData = JSON.parse(event.data);
      switch (eventData.type) {
        case 'CLEAR_CACHE':
          if (eventData.data.length) {
            caches.open('application-api').then((cache) => {
              // обрабатываем кеш например:
              // cache.AddAll(filesToCache); где filesToCache = ['/mypic.png', ...]
              eventData.data.forEach((url) => {
                cache.delete(url);
              });
            });
          }
          break;
        default:
          break;
      }
      /* const { categoriesItems } = data;
      if (categoriesItems && categoriesItems.length) {
        const exampleImage = 'https://aptiv.blob.core.windows.net/images/thumbnail_group_fce71228-3bd6-4013-ae1f-7f019a510eee_image.png';
        workbox.precaching.precache([
          { url: exampleImage },
        ]);
      } */
    } catch (e) {}
  });
}

