const webpack = require('webpack');
const path = require('path');
const autoprefixer = require('autoprefixer');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

module.exports = (env) => {
  const isProduction = env === 'production';

  const pluginsDefault = [
    new webpack.EnvironmentPlugin({
      'process.env.NODE_ENV': process.env.NODE_ENV || env || 'development',
    }),
    new webpack.NamedModulesPlugin(),
    new SpriteLoaderPlugin(),
  ];

  let plugins = [];

  if (isProduction) {
    plugins = [
      ...pluginsDefault,
      (new webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false,
          screw_ie8: true,
        },
        sourceMap: false,
      })),
      (new webpack.optimize.OccurrenceOrderPlugin()),
    ];
  } else {
    plugins = [
      ...pluginsDefault,
      // new BundleAnalyzerPlugin(),
      (new webpack.HotModuleReplacementPlugin()),
    ];
  }

  return {
    entry: [
      'babel-polyfill',
      './src/index.jsx',
    ],
    output: {
      path: path.join(__dirname, 'public'),
      // publicPath: '/',
      filename: 'bundle.js',
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      alias: {
        '~/api': path.resolve(__dirname, 'src/api/'),
        '~/components': path.resolve(__dirname, 'src/components/'),
        '~/containers': path.resolve(__dirname, 'src/containers/'),
        '~/stores': path.resolve(__dirname, 'src/stores/'),
        '~/constants': path.resolve(__dirname, 'src/constants/'),
        '~/router': path.resolve(__dirname, 'src/router/'),
        '~/config': path.resolve(__dirname, 'src/config/'),
        '~/style': path.resolve(__dirname, 'src/style/'),
        modernizr$: path.resolve(__dirname, '.modernizrrc'),
      },
    },
    module: {
      rules: [
        {
          test: /\.modernizrrc$/,
          loader: 'modernizr-loader!json-loader',
        },
        {
          test: /\.jsx?$/,
          use: [{
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
              plugins: ['react-hot-loader/babel'],
            },
          }, 'eslint-loader'],
          exclude: /node_modules/,
        }, {
          test: /\.scss$/,
          use: [{
            loader: 'style-loader', // creates style nodes from JS strings
          },
          {
            loader: 'css-loader', // translates CSS into CommonJS
            options: {
              sourceMap: !isProduction,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              plugins: [
                autoprefixer({
                  browsers: ['ie >= 9', 'last 4 version'],
                }),
              ],
              sourceMap: true,
            },
          }, {
            loader: 'sass-loader', // compiles Sass to CSS
            options: {
              sourceMap: !isProduction,
            },
          }],
        },
        {
          test: /\.(png|jpe?g|gif)$/,
          use: [{
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '/media/pic',
              publicPath: '/media/pic',
            },
          }],
        },
        {
          test: /\.(eot|otf|woff2?|ttf)$/,
          use: [{
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: '/media/fonts/',
              publicPath: '/media/fonts',
            },
          }],
        },
        {
          test: /\.svg$/,
          // exclude: /node_modules/,
          loaders: [
            `svg-sprite-loader?${JSON.stringify({
              name: '[name].[hash]',
              prefixize: true,
              // extract: true,
            })}`,
            `svgo-loader?${JSON.stringify({
              plugins: [
                { removeTitle: true },
                { convertPathData: false },
                { removeUselessStrokeAndFill: true },
                // { removeAttrs: { attrs: 'stroke.*' } },
              ],
            })}`,
          ],
          include: [
            path.resolve('src/style/svg'),
            path.resolve('node_modules/slick-carousel/slick/'),
          ],
        },
      ],
    },
    plugins,
    devtool: isProduction ? false : 'cheap-module-eval-source-map',
    devServer: {
      contentBase: path.join(__dirname, 'public'),
      historyApiFallback: true,
      compress: true,
      hot: true,
      // host: '192.168.9.169',
      host: '0.0.0.0',
      disableHostCheck: true,
      overlay: {
        errors: true,
        warnings: false,
      },
    },
  };
};
